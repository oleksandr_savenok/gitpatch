#!/bin/bash

root='/media/asavenok/secondary/android/sources/android-8.1.0_r7_2'
branch='android-8.1.0_r7'

path='platform/'

change_path() {

	echo ""
	echo ""

	full_path=$1
	platform_length=${#path}	

	cd ${root}
	cd ${full_path:platform_length}

	echo "change path to:"
	pwd

	git_status=$(git status 2>&1)
	if [[ $git_status =~ .*fatal.* ]]; then
		echo "error: not a git repositry."
		exit -1
	fi
}


apply_patch() {
	if [[ ${#1} == 0 ]]; then
		echo "skip empty line"
		return
	fi

	change_id=$(git show $1 | grep "Change-Id")
	change_id="$(echo -e "${change_id}" | tr -d '[:space:]')"
	semicolon_position=$(echo ${change_id} | grep -aob ':' | grep -oE '[0-9]+')
#	echo "position: $semicolon_position"
	change_id=${change_id:semicolon_position+1}
	echo "change_id: $change_id"

	log_result=$(git log --format=format:%H --grep $change_id) #"Change-Id: $1")	
	echo "log_result: $log_result"

	if [[ ${#log_result} > 0 ]]; then
		echo "patch with change-id: $change_id already mearged. skip."
		return
	fi

	echo "apply_patch: $1"

	cherry_pick_status=$(git cherry-pick ${1} 2>&1)

	if [[ $cherry_pick_status =~ .*fatal.* ]];  then
		echo "error: can't apply patch $1."
		exit -1
	else
		echo $cherry_pick_status
	fi
}

while IFS='' read -r line || [[ -n "$line" ]]; do
	if [[ ${line:0:1} == "#" ]]; then
		echo "skip commented line."
	elif [[ $line =~ .*${path}.* ]]; then
		change_path $line
	else
		apply_patch $line
	fi
done < "$1"
